<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="title" content="About Our Brand - Insurance Made Personal | Constantia Insurance Company">

  <meta name="description" content="When we decided to make a trusted claim to offer insurance made personal, we made it with the knowledge that we would deliver on that promise. The delivery on that promise is held in our people, both staff and clients, to whom small touches have always made the largest difference.">

  <link rel="icon" href="../assets/img/logos/favicon.png">

  <title>About Our Brand - Insurance Made Personal | Constantia Insurance Company</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
  <!-- jquery link -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Base styles for this template -->
  <link href="../css/interface.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/bootstrap-grid.css" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/responsive.css" rel="stylesheet">

  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
</head>
<body>
  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../inc/main-navigation.php'; ?>
  <main>
    <!-- header needs added background-image -->
    <header role="heading" id="heading">

      <div class="col-5 col-md-12 pt-3 px-3 pt-md-5 px-md-5 bg-cover-about" style="background-image:url('../assets/img/about/personalised-insurance-background.png');">
        <h6 style="color:#ea6908">WHO WE ARE</h6>
        <h1 style="color:white;width: 637px;">Insurance made personal<span class="h1" style="color:#ea6908">.</span></h1>
        <p style="color:#aeb5cb;">Being part of Constantia means you are part of that promise.Like the Seer, it means accepting a responsibility to look further, yet with greater focus, than any other insurer is able to.</p>
        <div class="pointer"></div>
      </div>
    </header>

    <!-- content 2 about    -->
    <section class="content-about">
      <div class="img-section">
        <span class="number">01</span>
        <h3>Being part of Constantia means you are part of that promise.</h3>
          <img class="img-02" src="../assets/img/about/tall-trees-constantia.png" alt="about-images" width="320" height="320">
          <img class="img-01" src="../assets/img/about/personalised-insurance-light-background-1.png" alt="about-images" width="200" height="200">
      </div>

      <div class="text-section">
        <h5>Like the Seer, it means accepting a responsibility to look further, yet with greater focus, than any other insurer is able to.</h5>
        <p>Being part of Constantia means knowing that our very smallest actions, our every seemingly insignificant point of behaviour, speaks volumes about our character and brand.
        </p>
        <p>This means that at every last point at which our company is able to speak about itself, in every email greeting, in every chair offered to a client, in every telephone answered and in every magazine article or radio advert we place, we need to take care of the detail in order to take care of the whole. Because, while we know that it is the seemingly small things that speak the largest volumes about us, it is never at the expense of losing sight of the larger vision.</p>
        <a href="#">A link to somewhere</a>
      </div>
    </section>


    <!-- the constantia story -->
    <section class="constantia-story jumborton text-center bg-cover " style="background-image:url('../assets/img/about/personalised-insurance-grey-background.png');">
      <span style="color:#ea6908">02</span><h2 style="color:white">The Constantia Story</h2>
      <h4>Our company’s story cannot be contained within a solitary caption, the margins of a single page, nor even within the covers of a single volume.
      </h4>
      <p>Our history more closely resembles a library, carefully hand selected over sixty years.</p>
      <p>
        Shelves of well-thumbed pages, re-read passages, and penciled-in liner notes. It can be seen in covers well-worn by the use of different hands, and in the way a reading chair remembers the shape of its most frequent users. It’s well-stocked with all the classics, but always with enough space reserved for newly acquired reading.</p>
        <p>
          While we know that wisdom comes with six decades of carefully-studied experience, it means nothing without a mind hungry for new knowledge.</p>
    </section>


        <!--directors   -->
        <section class="directors jumborton">
          <div class="row">
            <span class="number">03</span>
            <h2>Directors</h2>
          </div>
          <div class="row">
            <div class="director-img-01 col-md-3" style="height:250px;width:250px;">
              <img src="../assets/img/directors/constantia-chairman.png" alt="Stephen Richard Bruyns">
              <h4>Stephen Richard Bruyns</h4>
              <p>Chairman of the Board of Directors</p>
              <p>(Independent Non-Executive Director)</p>
              <a href="#">LEARN MORE</a>

            </div>
            <div class="director-img-02 col-md-3" style="height:250px;width:250px;">
              <img src="../assets/img/directors/constantia-chief-1.png" alt="Volker von Widdern">
              <h4>Volker von Widdern</h4>
              <p>Chief Executive Officer</p>
              <p>Group Risk Committee Member</p>
              <a href="#">LEARN MORE</a>

            </div>
            <div class="director-img-03 col-md-3" style="height:250px;width:250px;">
              <img src="../assets/img/directors/constantia-no-exe-1.png" alt="Gavin Toet">
              <h4>Gavin Toet</h4>
              <p>Non-Executive Director</p>
              <p>Remuneration Committee Member</p>
              <a href="#">LEARN MORE</a>

            </div>
            <div class="director-img-04 col-md-3" style="height:250px;width:250px;">
              <img src="../assets/img/directors/constantia-dir-1.png" alt="Jabulani Mahlangu">
              <h4>Jabulani Mahlangu</h4>
              <p>Independent Non-executive Director</p>
              <p>Group Audit Committee Member</p>
              <a href="#">LEARN MORE</a>

            </div>
          </div>
        </section>

        <section class="constantia-story jumborton text-center bg-cover" style="background-image:url('../assets/img/about/personalised-insurance-grey-background.png');height: 614px;margin-top: -20px;">
          <span style="color:#ea6908;margin-left: 134px;">04</span><h2 style="color:white">Complaints</h2>
          <p>Do you have any complaints regarding any Constantia Group Insurance matter?<br/>
            Do you feel you have been treated unfairly by your Insurance Broker or Underwriting Manager?<br/>
            Has your Insurance Broker or Underwriting Manager neglected to explain the terms and conditions of your policy to you?<br/>
            Are you concerned that your claim has been unfairly rejected?<br/>
            Do you feel there are any other areas where we could improve our service?<br/>
            If so, feel free to contact our Group Market Conduct Officer, Mrs Astrid Baynes on:</p>
            <p>
              Telephone Number: 011 686 4250 (direct line)<br/>
              Main Switchboard Number:  011 686 4200<br/>
              E-Mail Address: complaints@constantiagroup.co.za<br/>
              Physical Address: Tulbagh Office Park, 360 Oak Avenue, Randburg, 2194<br/>
              Postal Address: P O Box 3518, Cramerview, 2060<br/>
              As proof of our commitment to providing you with exceptional service, and in the unlikely event that our team has been unable to assist you, kindly contact Ms Jenny Ward,Personal Assistant to Mr Volker von Widdern, our Chief Executive Officer on:
            </p>
            <p>
              Telephone Number: 011 686 4309 (direct line)<br/>
              E-Mail address: jennyw@constantiagroup.co.za
            </p>
        </section>


        <!--top footer with licenses and constantia ltd links  -->
        <!--please note that the section needs a background-image  -->
        <section class="top-footer" style="background-image:url('../assets/img/background-images/about-bottom-bg.png');margin-top: 168px;">
              <div class="row">
                   <div class=" col-md-4">
                     <span>04</span>
                     <h3>Licenses</h3>
                     <p style="color:#c1c1c1;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>

                   </div>
                   <div class=" col-md-4" style="margin-left: 140px;">
                      <div class="top-footer-heading" style="color:white;font-size: 57px;font-family:'Merriweather', serif;line-height: 3.8rem;width: 597px;">Constantia Insurance Co Ltd<span class="span">→</span></div>
                      <p style="color:#818c95;width: 465px;">Chair remembers the shape of its most frequent users. Its well well-stocked with all the classics, but always with enough</p>

                      <hr></hr>

                      <!-- line comes here -->

                      <div class="about-para" style="color:#c2c5c9;font-size: 57px;font-family:'Merriweather', serif;line-height: 3.8rem;width: 512px;">Constantia Life and Health</div>
                   </div>
              </div>
        </section>

        </main>


        <!-- Include: Main Footer -->
        <?php include '../inc/main-footer.php'; ?>

        <script type="text/javascript">
        $(document).ready(function () {
          $(".content").hide();
          $(".show_hide").on("click", function () {
            var txt = $(".content").is(':visible') ? 'Read More' : 'Read Less';
            $(".show_hide").text(txt);
            $(this).next('.content').slideToggle(200);
          });
        });
        </script>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
        <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
        <script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

        <!-- Custom Interface JS -->
        <script src="../js/interface.js?v=2.0"></script>

        <!-- Placeholder script - remove prior to going live -->
        <!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

      </body>
      </html>
