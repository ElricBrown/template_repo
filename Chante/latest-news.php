<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../assets/img/logos/favicon.png">

  <title>Constantia | Insurance made personal</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
  <!-- Base styles for this template -->
  <link href="../css/interface.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/bootstrap-grid.css" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/responsive.css" rel="stylesheet">
  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
</head>
<body>
  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../inc/main-navigation.php'; ?>

  <!-- background-image to be added to the main -->
  <main>
    <header>
      <div class="row">
        <div class="col-md-6">

          <h1>INVESTOR INFORMATION</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
      </div>
    </header>

    <section>
      <h2>Corporate Social Responsibility</h2>
      <h3><b>2016 Initiatives</b></h3>
      <p>What did we do?<br/>
        On the 18th July 2016 the staff donated over 900 basic items of food,toiletries and clothing to Afrika Tikkun !</p>

        <h3><b>2015 Initiatives</b></h3>
        <p>The Constantia Group challenged its management and staff to contribute non-perishable food items and toiletry items, which will go towards our 67 minutes on Mandela Day, 18 July 2015.  A team of staff members delivered 1,100 items to Africa Tikkun and the children were absolutely overjoyed.</p>

        <h3><b>2014 Initiatives</b></h3>
        <p>During 2014 the Constantia Group supported the following initiatives:</p>
        <ul>
          <li>The Rotary Club</li>
          <li>Unsung Heroes</li>
          <li>SAIA Consumer Education </li>
        </ul>
        <p>The Group sponsors a Class Room with Africa Tikkun. Afrika Tikkun has enjoyed a wonderful partnership with Constantia Insurance Group, through the support of the Early Childhood Development Programme (ECD) at our Uthando Centre in Braamfontein. Through our donations, we have allowed Africa Tikkun to extend valuable foundational skills and knowledge to the disadvantaged children in the inner city Johannesburg community. Read more on the attached report from Africa Tikkun.</p>

        <h5>Media Documents</h5>
        <ul>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-corporate-social-responsibility-mandeladaycontributionsjuly2015.pdf">Mandela Day Contributions</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-corporate-social-responsibility.pdf">Africa Tikkun Uthando ECD Report</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-corporate-social-responsibility-letabafireprotection-trailersponsor.pdf">Letaba Fire Protection Trailer</a></li>
        </ul>
      </section>


      <section>
        <h2>Financial and Legal</h2>
        <h3>Access to Information</h3>
        <p>Please find our Access to information Summaries below.</p>
        <ul>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-access-to-information-accesstoinformationciclmar2015.pdf">Access to information summary - Constantia Insurance Company Limited</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-access-to-information-accesstoinformationclahmar2015.pdf">Access to information summary - Constantia Life and Health Assurance Company Limited</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-access-to-information-accesstoinformationcllmar2015.pdf">Access to information summary - Constantia Life Limited</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-access-to-information-accesstoinformationglasmar2015.pdf">Access to information summary - General Legal and Administration Services Limited</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-access-to-information-accesstoinformationhurrimar2015.pdf">Access to information summary - Hurriclaim (Pty) Limited</a></li>
        </ul>

        <div class="">
          <h3>Annual & Interim Reports</h3>
          <h4>Financials Current</h4>
          <p>The Constantia Group's Financial Year end is in June of every year.  The 2017 Annual Financial Statements are published herewith.</p>
          <ul>
            <li><a href="https://firebasestorage.googleapis.com/v0/b/constantiainsurance.appspot.com/o/Financial%20Reports%2FCICL_AFS_June_2017.pdf?alt=media&token=203fea61-bed6-4e9e-9149-32af6e595bb1">CICL Annual Financials 2017</a></li>
            <li><a href="https://firebasestorage.googleapis.com/v0/b/constantiainsurance.appspot.com/o/Financial%20Reports%2FCLL_AFS_June_2017.pdf?alt=media&token=f9694386-1122-49b2-bfc2-868b49c8a91e">CLL Annual Financials 2017</a></li>
            <li><a href="https://firebasestorage.googleapis.com/v0/b/constantiainsurance.appspot.com/o/Financial%20Reports%2FCLAH%20AFS%20June%202017.pdf?alt=media&token=177d5ec1-8313-4b7e-93c9-c18668157627">CLAH Annual Financials 2017</a></li>
          </ul>
          <h4>Financials Archive</h4>
          <p>The Constantia Group's Annual Financial Statements archives are published herewith.</p>
          <ul>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-current-ciclsignedafs2016.pdfCICL">Annual Financial Statements 2016</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-current-clahsignedafs2016.pdf">CLAH Annual Financial Statements 2016</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-current-cllsignedafs2016.pdf">CLL Annual Financial Statements 2016</a></li>
            <li><a href="https://firebasestorage.googleapis.com/v0/b/constantiainsurance.appspot.com/o/Financial%20Reports%2FCICL%20AFS%20June%202015.pdf?alt=media&token=1c4efa79-63c3-40da-9690-92af73602c0d">CICL Annual Financial Statements 2015</a></li>
            <li><a href="https://firebasestorage.googleapis.com/v0/b/constantiainsurance.appspot.com/o/Financial%20Reports%2FCLAH%20AFS%20June%202015.pdf?alt=media&token=b657691c-d086-4454-94c5-a195f9692e31">CLAH Annual Financial Statements 2015</a></li>
            <li><a href="https://firebasestorage.googleapis.com/v0/b/constantiainsurance.appspot.com/o/Financial%20Reports%2FCLL%20AFS%20June%202015.pdf?alt=media&token=79892ecf-e765-44d7-adbf-bf1b4d6a2ebf">CLL Annual Financial Statements 2015</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-archive-ciclsignedafs20141.pdf">CICL Annual Financials 2014</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-archive-clahsignedafs20141.pdf">CLAH Annual Financials 2014</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-archive-cllsignedafs20141.pdf">CLL Annual Financials 2014</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-archive-ciclsignedafs2013colour.pdf">CICL Annual Financial Statements 2013</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-archive-clahsignedafs2013colour.pdf">CLAH Annual Financial Statements 2013</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-archive-cllsignedafs2013colour.pdf">CLL Annual Financial Statements 2013</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-archive-ciclafs2012signedfinal271120120.pdf">CICL Annual Financial Statements 2012</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-archive-clahafs2012signedfinal071220120.pdf">CLAH Annual Financial Statements 2012</a></li>
            <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-financials-archive-cllafs2012signedfinal271120120.pdf">CLL Annual Financial Statements 2012</a></li>

          </ul>

          <h3>Group Figures (Fundamentals)</h3>
          <h4>Our Financials</h4>
          <p>At Constantia Insurance Group, we maintain financial integrity and strength in order to provide security and value for our clients and shareholders.</p>
          <img src="https://firebasestorage.googleapis.com/v0/b/constantiainsurance.appspot.com/o/Financial%20Reports%2FConstantia%20Group%20Figures%20-%20Fundamentals-1.jpg?alt=media&token=b0ae98d1-f7e5-4cb6-a660-58c87e9d3cd5" alt="" width="500px" height="500px">

        </div>

        <h2>Media Relations</h2>
        <h3>Featured Marketing Campaigns</h3>
        <p>Our campaigns.</p>
        <ul>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-featured-marketing-campaigns-constantiachampagneposter19may2014.pdf">Constantia Champagne Poster</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-featured-marketing-campaigns-0000constantianewposterillustrationsv3bs.pdf">Constantia Bull Poster</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-featured-marketing-campaigns-constantianeedleposter.pdf">Constantia Needle Poster</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-featured-marketing-campaigns-constantiapenposter.pdf">Constantia Pen Poster</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-featured-marketing-campaigns-constantiatieposter.pdf">Constantia Tie Poster</a></li>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-featured-marketing-campaigns-constantiatreeposter.pdf">Constantia Tree Poster</a></li>
        </ul>

        <h3>Featured News</h3>
        <p>Cover Magazine Feature - Issue September 2012</p>
        <ul>
          <li><a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-featured-news-covermagazinefeatureseptember2012.pdf">Cover Magazine Feature - September 2012</a></li>
        </ul>

        <h3>Press Releases</h3>
        <p>GCR upgrades Constantia Insurance Company Limited’s rating to A - <a href="https://firebasestorage.googleapis.com/v0/b/constantiainsurance.appspot.com/o/website_files%2FGCR_upgrades_Constantia_Insurance_Company_Limited_s_rating_to_A(ZA)__Outlook_Stable.pdf?alt=media&token=3b3fc499-24d4-4360-84dd-d9afc58906ba">read more</a></p>
        <p>Constantia welcomes its new Chairman and Board Memeber - <a href="https://www.constantiagroup.co.za/new-board-members">read more</a></p>
        <p>Knysna Bull gets personal with new title sponsor - <a href="http://knysnabull.bike/constantia-insurance/">read more</a></p>
        <p>Follow the link and see Constantia featured in the latest publication of IndustrySA (Issue 50) - <a href="http://issuu.com/industrysa/docs/isa_50/26?e=4879098/12668910">click here</a></p>
        <p>EFC and Constantia announces historic partnership - <a href="http://www.sportindustry.co.za/news/efc-announces-historic-partnership-constantia">read more</a></p>
      </section>

    </main>


    <!-- Include: Main Footer -->
    <?php include '../inc/main-footer.php'; ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

    <!-- Custom Interface JS -->
    <script src="../js/interface.js?v=2.0"></script>

    <!-- Placeholder script - remove prior to going live -->
    <!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

  </body>
  </html>
