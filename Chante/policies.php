<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../assets/img/logos/favicon.png">

  <title>Constantia | Insurance made personal</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

  <!-- Base styles for this template -->
  <link href="../css/interface.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/bootstrap-grid.css" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/responsive.css" rel="stylesheet">

  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>

  <style media="screen">

  </style>
</head>
<body>
  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../inc/main-navigation.php'; ?>

  <!-- background-image to be added to the main -->
  <main>
    <header>
      <div class="row">
        <div class="col-md-6">

          <h1>POLICIES</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
      </div>
    </header>

    <div class="row">
      <div class=" col-md-3">
        <h2>AA Policy Termination Letter</h2>
        <div class="">
          <p>Please download the Notice of Termination of your Insurance Driven by the AA Policy.</p>
          <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-aa-policy-termination-letter-aapolicyterminateletter2.pdf" target="_blank" >AA policy terminate letter.pdf</a>
        </div>
      </div>

      <div class=" col-md-3">
        <h2>Constantia Insurance Co Ltd</h2>
        <div class="">
          <p>Please download our Conflict of Interest Management Policy</p>
          <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-constantia-insurance-co-ltd-annexure7-conflictofinterestmanagementpolicy-signed.pdf" target="_blank">Constantia Insurance Company Conflict of Interest Management Policy</a>
        </div>
      </div>

      <div class=" col-md-3">
        <h2>Constantia Life Limited</h2>
        <div class="">
          <p>Please download our Conflict of Interest Management policy.</p>
          <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-constantia-life-limited-annexure7-conflictofinterestmanagementpolicy-signed0.pdf" target="_blank">Constantia Insurance Company Conflict of Interest Management Policy</a>
        </div>
      </div>


      <div class=" col-md-3">
        <h2>Constantia Life and Health</h2>
        <div class="">
          <p>Please download a copy of the Constantia Insurance Company Conflict of Interest Management Policy.</p>
          <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-constantia-life-and-health-annexure7-conflictofinterestmanagementpolicy-signed1.pdf" target="_blank">Company Conflict of Interest Management Policy</a>
        </div>
      </div>

    </div>

    <h2>ALL OUR POLICIES</h2>
    <p>Relating to the entities which are licenced Insurers and licenced Financial Service Providers in the Constantia Risk and Insurance Holdings (Pty) Ltd group of companies, and comprise:   </p>
    <ul>
      <li>CONSTANTIA INSURANCE COMPANY LTD,  FSP 31111</li>
      <li>CONSTANTIA LIFE LTD</li>
      <li>CONSTANTIA LIFE AND HEALTH ASSURANCE COMPANY LTD</li>
      <li>HURRICLAIM (PTY) LTD,  FSP 12912</li>
      <li>GENERAL LEGAL AND ADMINISTRATIVE SERVICES (PTY) LTD, FSP 6350</li>
    </ul>

    <div class="row pt-md-5">
      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-conflictofinterestmanagementpolicy-signed.pdf" target="_blank">Constantia Insurance Group Conflict of Interest Management Policy</a>
      </div>

      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-compliancegovernancepolicy-signed.pdf" target="_blank">Constantia Insurance Group Compliance Governance Policy</a>

      </div>

      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-internalauditpolicy-signed.pdf" target="_blank">Constantia Insurance Group Internal Audit Policy</a>

      </div>

      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-internalcontrolsgovernancepolicy-signed.pdf" target="_blank">Constantia Insurance Group Internal Controls Governance Policy</a>

      </div>

      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-investmentpolicy-signed.pdf" target="_blank">Constantia Insurance Group Investment Policy</a>

      </div>
    </div>

    <div class="row pt-md-5">
      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-outsourcingpolicy-signed.pdf" target="_blank">Constantia Insurance Group Outsourcing Policy</a>
      </div>

      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-fitproperpolicy-signed.pdf" target="_blank">Constantia Insurance Group Fit & Proper Policy</a>

      </div>

      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-reinsurancepolicy-signed.pdf" target="_blank">Constantia Insurance Group Reinsurance Policy</a>

      </div>

      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-remunerationpolicyv1apr13.pdf" target="_blank">Constantia Insurance Group Remuneration Policy</a>

      </div>

      <div class="col-md-2">
        <a href="https://www.constantiagroup.co.za/assets/sites/default/files/page-file-attachments/constantia-group-all-our-policies-complaintsresolutionprocedureversion3-march2017-signed.pdf" target="_blank">Constantia Insurance Group Complaints Resolution Policy</a>
      </div>
    </div>
  </main>


  <!-- Include: Main Footer -->
  <?php include '../inc/main-footer.php'; ?>


  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
  <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
  <script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

  <!-- Custom Interface JS -->
  <script src="../js/interface.js?v=2.0"></script>

  <!-- Placeholder script - remove prior to going live -->
  <!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

</body>
</html>
