<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="title" content="Our Directors - Insurance Made Personal | Constantia Insurance Company">

  <meta name="description" content="What is the real point of having these highly skilled people and agile way of doing business and simple systems to ensure our business gets done well? It means all of us accept a responsibility to look further. At Constantia we never lose sight of the larger vision. We never stop looking for new opportunities. And that is what we mean by “insurance made personal”.">

  <link rel="icon" href="../assets/img/logos/favicon.png">

  <title>Our Directors - Insurance Made Personal | Constantia Insurance Company</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
  <!-- jquery link -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Base styles for this template -->
  <link href="../css/interface.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/bootstrap-grid.css" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/responsive.css" rel="stylesheet">

  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
  <style media="screen">


  </style>
</head>
<body>
  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../inc/main-navigation.php'; ?>
  <main>

    <!-- header needs added background-image -->
    <header>
      <h6>WHO WE ARE</h6>
      <h1>Directors</h1>
    </header>

<!--start of tabs  -->

  <div class="tab-block" id = "tab-block">

    <ul class="tab-mnu">
      <li class = "active" ><h6>CONSTANTIA INSURANCE COMPANY LIMITED</h6></li>
      <li><h6>CONSTANTIA LIFE LIMITED</h6></li>
      <li><h6>CONSTANTIA LIFE & HEALTH</h6></li>
    </ul>

    <div class="tab-cont">
      <section class="tab-pane">
        <h1>Constantia Insurance Company Limited</h1>

        <section>

          <div class="row">
            <div class="director-img-01 col-md-3" >
              <img src="../assets/img/directors/constantia-chairman.png" alt="Constantia limited Chairman: Stephen Richard Bruyns">
              <h4>Stephen Richard Bruyns</h4>
              <p>Chairman of the Board of Directors
              </p>
              <p>(Independent Non-Executive Director)</p>
              <a href="#" class="Stephen_hide" data-content="toggle-text">LEARN MORE</a>
              <div class="content_stephen">
                <p>Richard’s qualifications include a CA (SA) and PDM (Harvard) and his directorship experience spans many industries and achievements.  He is currently the independent non-executive Chairman of MiX Telematics Limited and New Africa Investments Limited, both listed on the JSE, and Chairman of Carnelley Rangecroft Consultancy. Richard has, during the past 20 years, led companies employing from 500 to 12 000 employees and with annual sales ranging from R500 million to R20 billion in the IT, manufacturing, construction, hospitality and consumer goods industries. His experience includes turning one of Africa’s largest operators and managers of high-end bush lodges from losses to strong profitability and culminating with “best hotel in Africa and the Middle East” and “second best small hotel in the world 2005” for two of the group’s lodges. Richard has served on many boards, some of which include Malbak Limited, Kohler Packaging Limited, Kimberley Clark of SA, Crown Cork SA Proprietary Limited, Control Instruments Limited, Conservation Corporation of SA Limited and Shift Interactive Communications.</p>
              </div>
            </div>

            <div class="director-img-02 col-md-3" >
              <img src="../assets/img/directors/constantia-chief.png" alt="Constantia limited Chief Executive: Volker von Widdern">
              <h4>Volker von Widdern</h4>
              <p>Chief Executive Officer</p>
              <p>Group Risk Committee Member</p>
              <a href="#" class="volker_hide" data-content="toggle-text">LEARN MORE</a>
              <div class="content_volker">
                <p>Volker von Widdern (B.Acc CA (SA) MBA) joined the Constantia Group in July 2016 with an impressive financial and business background in a broad spectrum of insurance activities, focusing specifically on strategic and operational risk management, claims consulting (forensic accounting and claims services) , risk modelling and analytics, and Captive Insurance Company management. He began his insurance career in 1996 as Financial Manager of Guardrisk Insurance Company Limited (“Guardrisk”), expanding his activities to include operations and underwriting, as well as the servicing of a life and non-life marketing portfolio. He was subsequently appointed Managing Director of Guardrisk Group  in 1999.</p>
                <p>In October 2002, Mr von Widdern joined Marsh SA (“Marsh”) as a Director. He became a member of the Executive Committee and expanded his activities with Marsh into the global market where he was responsible for the strategic development of risk consulting services in Australasia, Africa, Latin America and the Middle East. These activities resulted in the development of several risk management models across various sectors, and the formation of both insurance products and insurance company structures on innovative bases. In 2013 IRMSA awarded the Risk manager of the Year award to Mr von Widdern. </p>
              </div>

            </div>


            <div class="director-img-04 col-md-3" >
              <img src="../assets/img/directors/constantia-non-exe.png" alt="Constantia limited Non-executive Director: Tyrone Christie Moodley">
              <h4>Tyrone Christie Moodley</h4>
              <p> Non-executive Director</p>
              <a href="#" class="tyrone_hide" data-content="toggle-text">LEARN MORE</a>
              <div class="content_tyrone">
                <p>Tyrone is a founder and Chief Executive Officer of Midbrook Lane Proprietary Limited, a private investment company. His career began at Sasfin Securities as a Research Analyst before he left to start Midbrook. Tyrone obtained a Bachelor of Commerce Degree from the University of Johannesburg and is a Senior Advisor to Protea Asset Management LLC, an investment advisor based in the United States. He has more than 10 years of investment and investment company experience.</p>
              </div>

            </div>


      <div class="director-img-04 col-md-3" >
        <img src="../assets/img/directors/constantia-dir.png" alt="Constantia limited Independent Non-executive Director: Jabulani Mahlangu">
        <h4>Jabulani Mahlangu</h4>
        <p>Independent Non-executive Director</p>
        <p>Group Audit Committee Member</p>
        <a href="#" class="jabulani_hide" data-content="toggle-text">LEARN MORE</a>
        <div class="content_jabulani">
          <p>Jabu completed his articles with PwC in 1996. He joined the Offices for Serius Economic Offences in 1998 and in 2000 returned to the offices of PwC where he was appointed as head of the PwC Forensic Services practice in Gauteng and was admitted as a partner in 2002. Jabu founded Ligwa Advisory Services and has a diverse client base. He has performed audit and forensic related assignments internationally and in addition carried out various statutory appointments. Jabu has served as Inspector of Companies in terms of the Companies Act, 61 of 1973, Curator in terms of the Financial Services Board Act, 97 of 1990 and as a curator bonis in terms of the Prevention of Organised Crime Act, 121 of 1998. Jabu holds a B.Com (Acc), B.Compt (Hons) and CTA, CA (SA) degree.</p>
        </div>
      </div>

    </div>
  </section>

  <section>

    <div class="row">
      <div class="director-img-01 col-md-3" >
        <img src="../assets/img/directors/constantia-non-dir.png" alt="Constantia limited Non-executive Director: Lourens Erasmus Louw">
        <h4>Lourens Erasmus Louw</h4>
        <p>Non-Executive Director</p>
        <a href="#" class="lourens_hide" data-content="toggle-text">LEARN MORE</a>
        <div class="content_lourens">
          <p>Lourens studied at the University of Stellenbosch, after which he moved to Johannesburg and qualified as a member of the SA Institute of Stockbrokers. In 1996 Lourens was appointed financial director and compliance officer of stock broking firm Irish & Menell Rosenberg (Pty) Ltd and its successors in the Appleton Group. He remained with Appleton until June 2003 when he joined Conduit Capital. Lourens became the financial director of Conduit Capital in October 2004.</p>
        </div>
      </div>

      <div class="director-img-02 col-md-3" >
        <img src="../assets/img/directors/constantia-com-chairman.png" alt="Constantia limited Group Audit Committee Chairperson and Independent Non-executive Director: Rosetta Ntambose Xaba">
        <h4>Rosetta Ntambose Xaba</h4>
        <p>Independent Non-Executive Director</p>
        <p>Group Audit Committee Chairperson</p>
        <a href="#" class="rosetta_hide" data-content="toggle-text">LEARN MORE</a>
        <div class="content_rosetta">
          <p>Rosetta is a charted accountant and a registered auditor. She served her articles and qualified with KPMG in 2001. She worked up to manager level in internal and external auditing. Rosetta then moved to Deloitte Consulting, where she worked as manager for the Business Process Outsourcing unit, which specialized in public sector entities. This is where she gained a valuable experience in the public sector industry. Rosetta is the founder and Director of Rossal No 98, a consulting, auditing and business advisory services company. She has been running the company since 2007. Through her company she is able to provide auditing, accounting, business advisory services to the public sector, SMMEs and NGOs clients. Rosetta is currently a non- executive director at Technology Innovation Hub, a public entity under the Department of Science and Technology. She is a Treasurer and past Chairman of the Little Eden Society for the Care of Persons with Mental Handicap. She is also a non-executive and chair of the audit committee for FINBOND Limited Group. It is through her involvement in FINBOND Group Limited that she gained experience in the regulatory and compliance environment of the financial services sector.</p>
        </div>
      </div>
    </div>
  </section>
  </section>



  <section class="tab-pane">
    <h1>Constantia Life Limited</h1>

    <section>

      <div class="row">
        <div class="director-img-01 col-md-3" >
          <img src="../assets/img/directors/constantia-chairman.png" alt="Constantia limited Chairman: Stephen Richard Bruyns">
          <h4>Stephen Richard Bruyns</h4>
          <p>Chairman of the Board of Directors
          </p>
          <p>(Independent Non-Executive Director)</p>
          <a href="#" class="Stephen_hide" data-content="toggle-text">LEARN MORE</a>
          <div class="content_stephen">
            <p>Richard’s qualifications include a CA (SA) and PDM (Harvard) and his directorship experience spans many industries and achievements.  He is currently the independent non-executive Chairman of MiX Telematics Limited and New Africa Investments Limited, both listed on the JSE, and Chairman of Carnelley Rangecroft Consultancy. Richard has, during the past 20 years, led companies employing from 500 to 12 000 employees and with annual sales ranging from R500 million to R20 billion in the IT, manufacturing, construction, hospitality and consumer goods industries. His experience includes turning one of Africa’s largest operators and managers of high-end bush lodges from losses to strong profitability and culminating with “best hotel in Africa and the Middle East” and “second best small hotel in the world 2005” for two of the group’s lodges. Richard has served on many boards, some of which include Malbak Limited, Kohler Packaging Limited, Kimberley Clark of SA, Crown Cork SA Proprietary Limited, Control Instruments Limited, Conservation Corporation of SA Limited and Shift Interactive Communications.</p>
          </div>
        </div>

        <div class="director-img-02 col-md-3" >
          <img src="../assets/img/directors/constantia-chief.png" alt="Constantia limited Chief Executive: Volker von Widdern">
          <h4>Volker von Widdern</h4>
          <p>Chief Executive Officer</p>
          <p>Group Risk Committee Member</p>
          <a href="#" class="volker_hide" data-content="toggle-text">LEARN MORE</a>
          <div class="content_volker">
            <p>Volker von Widdern (B.Acc CA (SA) MBA) joined the Constantia Group in July 2016 with an impressive financial and business background in a broad spectrum of insurance activities, focusing specifically on strategic and operational risk management, claims consulting (forensic accounting and claims services) , risk modelling and analytics, and Captive Insurance Company management. He began his insurance career in 1996 as Financial Manager of Guardrisk Insurance Company Limited (“Guardrisk”), expanding his activities to include operations and underwriting, as well as the servicing of a life and non-life marketing portfolio. He was subsequently appointed Managing Director of Guardrisk Group  in 1999.</p>
            <p>In October 2002, Mr von Widdern joined Marsh SA (“Marsh”) as a Director. He became a member of the Executive Committee and expanded his activities with Marsh into the global market where he was responsible for the strategic development of risk consulting services in Australasia, Africa, Latin America and the Middle East. These activities resulted in the development of several risk management models across various sectors, and the formation of both insurance products and insurance company structures on innovative bases. In 2013 IRMSA awarded the Risk manager of the Year award to Mr von Widdern. </p>
          </div>

        </div>


        <div class="director-img-04 col-md-3" >
          <img src="../assets/img/directors/constantia-non-exe.png" alt="Constantia limited Non-executive Director: Tyrone Christie Moodley">
          <h4>Tyrone Christie Moodley</h4>
          <p> Non-executive Director</p>
          <a href="#" class="tyrone_hide" data-content="toggle-text">LEARN MORE</a>
          <div class="content_tyrone">
            <p>Tyrone is a founder and Chief Executive Officer of Midbrook Lane Proprietary Limited, a private investment company. His career began at Sasfin Securities as a Research Analyst before he left to start Midbrook. Tyrone obtained a Bachelor of Commerce Degree from the University of Johannesburg and is a Senior Advisor to Protea Asset Management LLC, an investment advisor based in the United States. He has more than 10 years of investment and investment company experience.</p>
          </div>

        </div>
        <div class="director-img-03 col-md-3" >
        <img src="../assets/img/directors/constantia-no-exe.png" alt="Constantia limited Non-executive Director: Gavin Toet">
        <h4>Gavin Toet</h4>
        <p>Non-Executive Director</p>
        <p>Remuneration Committee Member</p>
        <a href="#" class="gavin_hide" data-content="toggle-text">LEARN MORE</a>
        <div class="content_gavin">
        <p>Gavin was involved in the printing industry for 6 years. In 1999, he joined the Altmedia Group, where he ultimately held the position of General Manager. Gavin joined Conduit Capital in June 2005 and currently holds the position of Executive Director. He carries out a broad range of responsibilities, mainly operational in nature, and serves on numerous Boards and Executive Management Committees within the Conduit Group.
      </p>
    </div>
  </div>

  <div class="director-img-04 col-md-3" >
    <img src="../assets/img/directors/constantia-dir.png" alt="Constantia limited Independent Non-executive Director: Jabulani Mahlangu">
    <h4>Jabulani Mahlangu</h4>
    <p>Independent Non-executive Director</p>
    <p>Group Audit Committee Member</p>
    <a href="#" class="jabulani_hide" data-content="toggle-text">LEARN MORE</a>
    <div class="content_jabulani">
      <p>Jabu completed his articles with PwC in 1996. He joined the Offices for Serius Economic Offences in 1998 and in 2000 returned to the offices of PwC where he was appointed as head of the PwC Forensic Services practice in Gauteng and was admitted as a partner in 2002. Jabu founded Ligwa Advisory Services and has a diverse client base. He has performed audit and forensic related assignments internationally and in addition carried out various statutory appointments. Jabu has served as Inspector of Companies in terms of the Companies Act, 61 of 1973, Curator in terms of the Financial Services Board Act, 97 of 1990 and as a curator bonis in terms of the Prevention of Organised Crime Act, 121 of 1998. Jabu holds a B.Com (Acc), B.Compt (Hons) and CTA, CA (SA) degree.</p>
    </div>
  </div>

</div>
</section>

<section>

<div class="row">
  <!-- <div class="director-img-01 col-md-3" >
    <img src="" alt="">
    <h4>Lourens Erasmus Louw</h4>
    <p>Non-Executive Director</p>
    <a href="#" class="lourens_hide" data-content="toggle-text">LEARN MORE</a>
    <div class="content_lourens">
      <p>Lourens studied at the University of Stellenbosch, after which he moved to Johannesburg and qualified as a member of the SA Institute of Stockbrokers. In 1996 Lourens was appointed financial director and compliance officer of stock broking firm Irish & Menell Rosenberg (Pty) Ltd and its successors in the Appleton Group. He remained with Appleton until June 2003 when he joined Conduit Capital. Lourens became the financial director of Conduit Capital in October 2004.</p>
    </div>
  </div> -->

  <div class="director-img-02 col-md-3" >
    <img src="../assets/img/directors/constantia-com-chairman.png" alt="Constantia limited Group Audit Committee Chairperson and Independent Non-executive Director: Rosetta Ntambose Xaba">
    <h4>Rosetta Ntambose Xaba</h4>
    <p>Independent Non-Executive Director</p>
    <p>Group Audit Committee Chairperson</p>
    <a href="#" class="rosetta_hide" data-content="toggle-text">LEARN MORE</a>
    <div class="content_rosetta">
      <p>Rosetta is a charted accountant and a registered auditor. She served her articles and qualified with KPMG in 2001. She worked up to manager level in internal and external auditing. Rosetta then moved to Deloitte Consulting, where she worked as manager for the Business Process Outsourcing unit, which specialized in public sector entities. This is where she gained a valuable experience in the public sector industry. Rosetta is the founder and Director of Rossal No 98, a consulting, auditing and business advisory services company. She has been running the company since 2007. Through her company she is able to provide auditing, accounting, business advisory services to the public sector, SMMEs and NGOs clients. Rosetta is currently a non- executive director at Technology Innovation Hub, a public entity under the Department of Science and Technology. She is a Treasurer and past Chairman of the Little Eden Society for the Care of Persons with Mental Handicap. She is also a non-executive and chair of the audit committee for FINBOND Limited Group. It is through her involvement in FINBOND Group Limited that she gained experience in the regulatory and compliance environment of the financial services sector.</p>
    </div>
  </div>
</div>
</section>
</section>



  <section class="tab-pane">
    <h1>Constantia Life & Health </h1>

    <section>

      <div class="row">
        <div class="director-img-01 col-md-3" >
          <img src="../assets/img/directors/constantia-chairman.png" alt="Constantia limited Chairman: Stephen Richard Bruyns">
          <h4>Stephen Richard Bruyns</h4>
          <p>Chairman of the Board of Directors
          </p>
          <p>(Independent Non-Executive Director)</p>
          <a href="#" class="Stephen_hide" data-content="toggle-text">LEARN MORE</a>
          <div class="content_stephen">
            <p>Richard’s qualifications include a CA (SA) and PDM (Harvard) and his directorship experience spans many industries and achievements.  He is currently the independent non-executive Chairman of MiX Telematics Limited and New Africa Investments Limited, both listed on the JSE, and Chairman of Carnelley Rangecroft Consultancy. Richard has, during the past 20 years, led companies employing from 500 to 12 000 employees and with annual sales ranging from R500 million to R20 billion in the IT, manufacturing, construction, hospitality and consumer goods industries. His experience includes turning one of Africa’s largest operators and managers of high-end bush lodges from losses to strong profitability and culminating with “best hotel in Africa and the Middle East” and “second best small hotel in the world 2005” for two of the group’s lodges. Richard has served on many boards, some of which include Malbak Limited, Kohler Packaging Limited, Kimberley Clark of SA, Crown Cork SA Proprietary Limited, Control Instruments Limited, Conservation Corporation of SA Limited and Shift Interactive Communications.</p>
          </div>
        </div>

        <div class="director-img-02 col-md-3" >
          <img src="../assets/img/directors/constantia-chief.png" alt="Constantia limited Chief Executive: Volker von Widdern">
          <h4>Volker von Widdern</h4>
          <p>Chief Executive Officer</p>
          <p>Group Risk Committee Member</p>
          <a href="#" class="volker_hide" data-content="toggle-text">LEARN MORE</a>
          <div class="content_volker">
            <p>Volker von Widdern (B.Acc CA (SA) MBA) joined the Constantia Group in July 2016 with an impressive financial and business background in a broad spectrum of insurance activities, focusing specifically on strategic and operational risk management, claims consulting (forensic accounting and claims services) , risk modelling and analytics, and Captive Insurance Company management. He began his insurance career in 1996 as Financial Manager of Guardrisk Insurance Company Limited (“Guardrisk”), expanding his activities to include operations and underwriting, as well as the servicing of a life and non-life marketing portfolio. He was subsequently appointed Managing Director of Guardrisk Group  in 1999.</p>
            <p>In October 2002, Mr von Widdern joined Marsh SA (“Marsh”) as a Director. He became a member of the Executive Committee and expanded his activities with Marsh into the global market where he was responsible for the strategic development of risk consulting services in Australasia, Africa, Latin America and the Middle East. These activities resulted in the development of several risk management models across various sectors, and the formation of both insurance products and insurance company structures on innovative bases. In 2013 IRMSA awarded the Risk manager of the Year award to Mr von Widdern. </p>
          </div>

        </div>


        <div class="director-img-04 col-md-3" >
          <img src="../assets/img/directors/constantia-non-exe.png" alt="Constantia limited Non-executive Director: Tyrone Christie Moodley">
          <h4>Tyrone Christie Moodley</h4>
          <p> Non-executive Director</p>
          <a href="#" class="tyrone_hide" data-content="toggle-text">LEARN MORE</a>
          <div class="content_tyrone">
            <p>Tyrone is a founder and Chief Executive Officer of Midbrook Lane Proprietary Limited, a private investment company. His career began at Sasfin Securities as a Research Analyst before he left to start Midbrook. Tyrone obtained a Bachelor of Commerce Degree from the University of Johannesburg and is a Senior Advisor to Protea Asset Management LLC, an investment advisor based in the United States. He has more than 10 years of investment and investment company experience.</p>
          </div>

        </div>
        <div class="director-img-03 col-md-3" >
        <img src="../assets/img/directors/constantia-no-exe.png" alt="Constantia limited Non-executive Director: Gavin Toet">
        <h4>Gavin Toet</h4>
        <p>Non-Executive Director</p>
        <p>Remuneration Committee Member</p>
        <a href="#" class="gavin_hide" data-content="toggle-text">LEARN MORE</a>
        <div class="content_gavin">
        <p>Gavin was involved in the printing industry for 6 years. In 1999, he joined the Altmedia Group, where he ultimately held the position of General Manager. Gavin joined Conduit Capital in June 2005 and currently holds the position of Executive Director. He carries out a broad range of responsibilities, mainly operational in nature, and serves on numerous Boards and Executive Management Committees within the Conduit Group.
      </p>
    </div>
  </div>

  <div class="director-img-04 col-md-3" >
    <img src="../assets/img/directors/constantia-dir.png" alt="Constantia limited Independent Non-executive Director: Jabulani Mahlangu">
    <h4>Jabulani Mahlangu</h4>
    <p>Independent Non-executive Director</p>
    <p>Group Audit Committee Member</p>
    <a href="#" class="jabulani_hide" data-content="toggle-text">LEARN MORE</a>
    <div class="content_jabulani">
      <p>Jabu completed his articles with PwC in 1996. He joined the Offices for Serius Economic Offences in 1998 and in 2000 returned to the offices of PwC where he was appointed as head of the PwC Forensic Services practice in Gauteng and was admitted as a partner in 2002. Jabu founded Ligwa Advisory Services and has a diverse client base. He has performed audit and forensic related assignments internationally and in addition carried out various statutory appointments. Jabu has served as Inspector of Companies in terms of the Companies Act, 61 of 1973, Curator in terms of the Financial Services Board Act, 97 of 1990 and as a curator bonis in terms of the Prevention of Organised Crime Act, 121 of 1998. Jabu holds a B.Com (Acc), B.Compt (Hons) and CTA, CA (SA) degree.</p>
    </div>
  </div>

</div>
</section>

<section>

<div class="row">
  <!-- <div class="director-img-01 col-md-3" >
    <img src="" alt="">
    <h4>Lourens Erasmus Louw</h4>
    <p>Non-Executive Director</p>
    <a href="#" class="lourens_hide" data-content="toggle-text">LEARN MORE</a>
    <div class="content_lourens">
      <p>Lourens studied at the University of Stellenbosch, after which he moved to Johannesburg and qualified as a member of the SA Institute of Stockbrokers. In 1996 Lourens was appointed financial director and compliance officer of stock broking firm Irish & Menell Rosenberg (Pty) Ltd and its successors in the Appleton Group. He remained with Appleton until June 2003 when he joined Conduit Capital. Lourens became the financial director of Conduit Capital in October 2004.</p>
    </div>
  </div> -->

  <div class="director-img-02 col-md-3" >
    <img src="../assets/img/directors/constantia-com-chairman.png" alt="Constantia limited Group Audit Committee Chairperson and Independent Non-executive Director: Rosetta Ntambose Xaba">
    <h4>Rosetta Ntambose Xaba</h4>
    <p>Independent Non-Executive Director</p>
    <p>Group Audit Committee Chairperson</p>
    <a href="#" class="rosetta_hide" data-content="toggle-text">LEARN MORE</a>
    <div class="content_rosetta">
      <p>Rosetta is a charted accountant and a registered auditor. She served her articles and qualified with KPMG in 2001. She worked up to manager level in internal and external auditing. Rosetta then moved to Deloitte Consulting, where she worked as manager for the Business Process Outsourcing unit, which specialized in public sector entities. This is where she gained a valuable experience in the public sector industry. Rosetta is the founder and Director of Rossal No 98, a consulting, auditing and business advisory services company. She has been running the company since 2007. Through her company she is able to provide auditing, accounting, business advisory services to the public sector, SMMEs and NGOs clients. Rosetta is currently a non- executive director at Technology Innovation Hub, a public entity under the Department of Science and Technology. She is a Treasurer and past Chairman of the Little Eden Society for the Care of Persons with Mental Handicap. She is also a non-executive and chair of the audit committee for FINBOND Limited Group. It is through her involvement in FINBOND Group Limited that she gained experience in the regulatory and compliance environment of the financial services sector.</p>
    </div>
  </div>
</div>
</section>
</section>
</div>
</main>

<?php include '../inc/main-footer.php'; ?>


<!-- jquery for all the LEARN MORE buttons -->


<!-- JavaScript for the tabs for different directors in different areas -->
<script type="text/javascript">
// Stephens read More
$(document).ready(function () {
  $(".content_stephen").hide();
  $(".Stephen_hide").on("click", function () {
    var txt = $(".content_stephen").is(':visible') ? 'LEARN MORE' : 'Read Less';
    $(".Stephen_hide").text(txt);
    $(this).next('.content_stephen').slideToggle(200);
  });
});

// volker read more
$(document).ready(function () {
  $(".content_volker").hide();
  $(".volker_hide").on("click", function () {
    var txt = $(".content_volker").is(':visible') ? 'LEARN MORE' : 'Read Less';
    $(".volker_hide").text(txt);
    $(this).next('.content_volker').slideToggle(200);
  });
});

// gavin read MORE

$(document).ready(function () {
  $(".content_gavin").hide();
  $(".gavin_hide").on("click", function () {
    var txt = $(".content_gavin").is(':visible') ? 'LEARN MORE' : 'Read Less';
    $(".gavin_hide").text(txt);
    $(this).next('.content_gavin').slideToggle(200);
  });
});

// jabulani read MORE

$(document).ready(function () {
  $(".content_jabulani").hide();
  $(".jabulani_hide").on("click", function () {
    var txt = $(".content_jabulani").is(':visible') ? 'LEARN MORE' : 'Read Less';
    $(".jabulani_hide").text(txt);
    $(this).next('.content_jabulani').slideToggle(200);
  });
});

// Lourens read MORE

$(document).ready(function () {
  $(".content_lourens").hide();
  $(".lourens_hide").on("click", function () {
    var txt = $(".content_lourens").is(':visible') ? 'LEARN MORE' : 'Read Less';
    $(".lourens_hide").text(txt);
    $(this).next('.content_lourens').slideToggle(200);
  });
});

// Rosetta read MORE

$(document).ready(function () {
  $(".content_rosetta").hide();
  $(".rosetta_hide").on("click", function () {
    var txt = $(".content_rosetta").is(':visible') ? 'LEARN MORE' : 'Read Less';
    $(".rosetta_hide").text(txt);
    $(this).next('.content_rosetta').slideToggle(200);
  });
});
// tyrone read MORE
$(document).ready(function () {
  $(".content_tyrone").hide();
  $(".tyrone_hide").on("click", function () {
    var txt = $(".content_tyrone").is(':visible') ? 'LEARN MORE' : 'Read Less';
    $(".tyrone_hide").text(txt);
    $(this).next('.content_tyrone').slideToggle(200);
  });
});

// tab menu
$(document).ready(function(){

  var tabWrapper = $('#tab-block'),
  tabMnu = tabWrapper.find('.tab-mnu  li'),
  tabContent = tabWrapper.find('.tab-cont > .tab-pane');

  tabContent.not(':first-child').hide();

  tabMnu.each(function(i){
    $(this).attr('data-tab','tab'+i);
  });
  tabContent.each(function(i){
    $(this).attr('data-tab','tab'+i);
  });

  tabMnu.click(function(){
    var tabData = $(this).data('tab');
    tabWrapper.find(tabContent).hide();
    tabWrapper.find(tabContent).filter('[data-tab='+tabData+']').show();
  });

  $('.tab-mnu > li').click(function(){
    var before = $('.tab-mnu li.active');
    before.removeClass('active');
    $(this).addClass('active');
  });

});
</script>
<!-- Include: Main Footer -->
<?php include '../inc/main-footer.php'; ?>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!-- Custom Interface JS -->
<script src="../js/interface.js?v=2.0"></script>

<!-- Placeholder script - remove prior to going live -->
<!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

</body>
</html>
