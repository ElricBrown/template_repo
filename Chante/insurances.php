<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">

  <meta name="title" content="Customised Insurance For You - Types of Insurance | Constantia Insurance Company">

  <meta name="description" content="Business Insurance, Legal Insurance, Accident Insurance, Personal Asset Insurance, Health Insurance, Life & Funeral Insurance and more customised specifically for you.">
  <link rel="icon" href="../assets/img/logos/favicon.png">


  <title>Customised Insurance For You - Types of Insurance | Constantia Insurance Company</title>
  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

  <!-- Base styles for this template -->
  <link href="../css/interface.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/bootstrap-grid.css" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/responsive.css" rel="stylesheet">

  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
</head>


<body>
  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../inc/main-navigation.php'; ?>

  <!-- background-image to be added to the main -->
  <main>
    <header>
      <h1>Types-of-Insurance</h1>
    </header>

    <h2>Busines Insurance</h2>
    <h3>Agricultural Insurance</h3>
    <p>Agricultural Insurance
      Thank you for your interest in Agricultural Insurance. The content for this page is not quite ready however please feel free to contact us for further information.</p>


      <h3>Public Transport Insurance</h3>
      <p>Thank you for your interest in Public Transport Insurance. The content for this page is not quite ready however please feel free to contact us for further information.</p>

      <h2>Transport Insurance Underwriters</h2>
      <p>HCV Insurance Specialists ,We cater for the transporting insurance needs of the individual Owner-Driver to those of major Transport Carriers throughout South Africa and pride ourselves on offering customised insurance solutions at competitive rates and a superior level of service.</p>

      <p>The TIU team collectively has over 25 years in the HCV insurance industry and understands the needs of the trucking industry. So if you only have one truck exceeding 3500 Kgs to insure doing local deliveries, or have a fleet of 500 mechanical horses and trailers crisscrossing the country and neighbouring territories , then we’re the people you should be talking to.</p>

      <p>We offer cover for Heavy Trucks, including Goods in Transit insurance on the loads, and can include the Company’s cars, LDV’s and other commercial vehicles as well - or even cover the load on its own if required.</p>

      <p>Our offering includes the usual range of excess buy-down extensions, including electrical and mechanical breakdown towing cover.</p>

      <p>We have an excellent Roadside Assistance programme which includes flat tyre / flat battery /recovery and winching assistance, as well as a fuel delivery and vehicle protection service etc.</p>

      <p>If you would like to know more about TIU, or would like us to call on you to discuss an agency, please contact Rob Read on 083 226 6303, or write to him on rob@transportunderwriters.co.za</p>

      <p>We presently operate from premises in Centurion, at Tel No 012 942 4537 but will be relocating to offices in Roodepoort in 2017.</p>
      <p>Tel No 012 942 4537<br/>
        www.transportunderwriters.co.za</p>


        <h3>SGI Guarantee Acceptances</h3>
        <p>Insurance Guarantees and Bonds<br/>
           SGIGA was established in 1987 by Robert L Shaw. SGIGA were the Underwriting Managers of all guarantee and bond business for The Standard General Insurance Co. Ltd ("Stangen").During 1999, Stangen was bought by Guardian National Ltd who, in turn, taken over by Santam Ltd. In 1999 SGIGA moved to Constantia Insurance Company Ltd. In 2013 the business was divisionalised.</p>
        <p>SGIGA specialises in the following:</p>
        <p>Contractor's guarantees i.e. performance guarantees, retention guarantees, advance payment guarantees, development & reticulation bonds etc. Solvency guarantees i.e. payment guarantees, customs bonds, electricity supply bonds.Court Bonds for liquidated or insolvent estates.Educational Guarantees.Mining Rehabilitation Guarantees.</p>
        <p>www.sgiga.co.za<br/>
        CALL US ON 011 789 3352</p>

        <h2>Legal Insurance</h2>

        <h3>Medical Malpractice</h3>
        <p>Ethiqal Medical Risk Protection Ethiqal is a comprehensive medical indemnity solution developed for South African Health Practitioners by Constantia Insurance Company Limited. Ethiqal offers quality indemnity cover with the option of occurrence or claims made cover. Cover includes expert 24/7 Medico Legal support, mediation and comprehensive legal representation. This is supported by a 24/7 counselling service to help medical practitioners cope with the psychological challenges following complaints and claims. Ethiqal’s risk management philosophy includes a wide range of support services such as newsletters, case studies, practice risk management, peer reviews and mentoring. Ethiqal is truly a innovative South African solution by a South African insurer for South African Practitioners.</p>
        <p>For more information kindly contact us on:</p>
        <p>info@ethiqal.co.za<br/>
          www.ethiqal.co.za</p>

          <h3>Legal Expense Insurance</h3>
          <p>Legal Expense Insurance, Legal Defender,Legal Costs and Expenses Insurance,Legal Defender incorporates Legal Nexus and Prepaid Legal, and specialises in legal costs and expenses insurance and is currently serving approximately 25 000 policyholders countrywide.</p>
          <p>We are committed to service excellence and it is our mission to render the best legal advice and services to the peoples of South Africa in a friendly, effective and professional manner. Our vision is to ensure more South Africans access to the justice system, hence embracing one of the most important basic human rights as guaranteed by our Constitution.</p>
          <p>Please take a look at our Website for more information.</p>
          <p>www.legaldefender.co.za<br/>
            CALL US ON 0861 333 363</p>

            <h2>Accident Insurance</h2>

            <h3>Personal Accident</h3>
            <p>With sixty years of experience in financial services, Constantia Insurance Company Limited's direct marketing division, DuePoint, has always been focused on making financial services personal and is able to leverage its six decades of experience by offering true financial freedom for all South Africans.</p>
            <p>DuePoint's offices are located in the heart of Bryanston in Sandton, Johannesburg with easy access from the N1 via the William Nicol off-ramp. The offices are specifically designed for the needs of our Wealth Engineers creating an environment that is both aspirational and functional, offering fully equipped presentation facilities and ample space to connect with your peers</p>
            <p>DuePoint understands the unique pressures and financial needs facing South African families today. Our foundation principle is to ensure that South Africans get the protection they need, reduce or eliminate their household debt and offer them the financial training they need to build a successful channel with DuePoint, changing their lives forever.</p>

            <p>The DuePoint system affords individuals the opportunity to build a life time mechanism of additional income without the usual concerns and costs of starting your own business. Catering for both those who wish to merely supplement their existing income and those who wish to become truly financially independent, DuePoint offers the most innovative solution currently available in South Africa.</p>
            <p>www.duepoint0.net<br/>
              CALL US ON 010 020 4500</p>

              <h2>Personal Asset Insurance</h2>
              <h3>Mechanical Warranty Protection</h3>
              <p>Thank you for your interest in Mechanical Warranty Protection. The content for this page is not quite ready however please feel free to contact us for further information.</p>

              <h3>Mobile Consumer Products</h3>
              <p>Electronic Devices Our dedicated underwriting specialists offer an impressive basket of mobile handset insurance products aimed at the individual and corporate market.</p>
              <p>Cover extends to accidental, unforeseen physical loss or damage to the handset occurring anywhere in the world.</p>
              <p>Our insurance partner: Administration Plus (Pty) Ltd</p>
              <p>Go to List of Underwriters</p>

              <h2>Health Insurance</h2>

              <h3>Health and Accident</h3>
              <p>Thank you for your interest in Health and Accident Insurance. The content for this page is not quite ready however please feel free to contact us for further information.</p>

              <h3>Non-RSA Resident</h3>
              <p>Thank you for your interest in Health Insurance. The content for this page is not quite ready however please feel free to contact us for further information.</p>

              <h2>Life & Funeral Insurance</h2>

              <h3>Credit Life and Extended Warranty</h3>
              <p>Thank you for your interest in our Credit Life and Extended Warranty products. The content for this page is not quite ready however please feel free to contact us for further information.</p>

              <h3>Dealers Indemnity</h3>
              <p>Dealers Indemnity<br/>
                 Consumer Protection Insurance<br/>
                 Dealers Indemnity (Pty) Ltd ("DI") was established in 1993 with the sole purpose of specialising in Consumer Protection Insurance ("CPI"). DI is a member of SAUMA – South African Underwriting Managers Association. DI became a Division of Constantia Insurance Company Limited in September 2015.</p>
              <p>I has built a solid reputation and is known in the market for its commitment to personal service. As far as is known DI is the only brokerage in SA which specialises in Consumer Protection Insurance i.e. Death/Disability/Material Damage/Retrenchment/Extended Warranty specifically in the Retail Furniture and Micro Loan Industry.</p>

              <h3>Funeral and Credit Life Insurance</h3>
              <p>Thank you for your interest in our Funeral and Credit Life Insurance products. The content for this page is not quite ready however please feel free to contact us for further information</p>

              <h3>Life Assurance</h3>
              <p>Thank you for your interest in our Life Assurance products. The content for this page is not quite ready however please feel free to contact us for further information.</p>
      </main>

      <!-- Include: Main Footer -->
      <?php include '../inc/main-footer.php'; ?>


      <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
      <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
      <script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

      <!-- Custom Interface JS -->
      <script src="../js/interface.js?v=2.0"></script>

      <!-- Placeholder script - remove prior to going live -->
      <!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

    </body>


    </html>
