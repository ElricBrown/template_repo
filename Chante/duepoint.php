<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../assets/img/logos/favicon.png">

  <title>Constantia | Insurance made personal</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

  <!-- Base styles for this template -->
  <link href="../css/interface.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/bootstrap-grid.css" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/responsive.css" rel="stylesheet">


  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
</head>
<body>
  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../inc/main-navigation.php'; ?>

  <!-- background-image to be added to the main -->
  <main>

<header>
  <div class="row">
<div class="col-md-6">
  <img src="https://www.duepoint.net/Content/images/duepoint-logo.svg" alt="">
  <h1>Financial Freedom Realised<span>.</span></h1>
  <p>DuePoint understands the unique pressures and financial needs facing South African families today. Our foundation principle is to ensure that South Africans get the protection they need, reduce or eliminate their household debt and offer them the financial training they need to build a successfull channel with DuePoint changing their lives forever. </p>
  <a href="#">VISIT DUEPOINT WEBSITE</a>
</div>
<div class="col-md-6">
</div>
</div>
</header>

<section class="row">
  <div class="col">
    <span>01</span>
    <h2>"Chaos was the law of nature.Orderis the dream of man." - Henry Adams</h2>
  <div class="">

  </div>
  </div>

  <div class="text col">
    <h2> To represent these ideals, the DuePoint team realised they were trying to converge four very seperate objectives into one. A marriage of chaos into order. A convergence of destiny for its members.</h2>
    <p>Being part of Constantia means knowing that our very smallest actions, our every seemingly insignificant point of behaviour, speaks volumes about our character and brand.
    </p>
    <p>This means that at every last point at which our company is able to speak about itself, in every email greeting, in every chair offered to a client, in every telephone answered and in every magazine article or radio advert we place, we need to take care of the detail in order to take care of the whole. Because, while we know that it is the seemingly small things that speak the largest volumes about us, it is never at the expense of losing sight of the larger vision.</p>
    <a href="#">Visit the DuePoint Site</a>
  </div>
</section>

<section>
  <div class="row">
    <span>02</span>
    <h2>The DuePoint Team</h2>
  </div>
  <div class="row">
    <a href="#">VIEW MORE</a>
    <div class="director-img-01 col-md-3">
      <img src="" alt="">
      <h4>Brendan Benfield</h4>
      <p>Brendan is a registered Chartered Accountant and visiting fellow of Massachusetts...
      </p>
      <a href="#">LEARN MORE</a>

    </div>
    <div class="director-img-02 col-md-3">
      <img src="" alt="">
      <h4>Timothy Reynold</h4>
      <p>Timothy is a registered Chartered Accountant with over a decade of experience in the...</p>
      <a href="#">LEARN MORE</a>

    </div>
    <div class="director-img-03 col-md-3">
      <img src="" alt="">
      <h4>Rob van der Bijl</h4>
      <p>Rob was a co-founder of Wilred and has two decades of experinces in the digital marketing...</p>
      <a href="#">LEARN MORE</a>

    </div>
    <div class="director-img-04 col-md-3">
      <img src="" alt="">
      <h4>Stacey Paul</h4>
      <p>Stacey is a registered Chartered Accountant with both local and international experience across...</p>
      <a href="#">LEARN MORE</a>

    </div>
  </div>
</section>

<section>
  <div class="row">
    <div class="col-md-4">
    <span>03</span>
    <h2>DuePoint Products</h2>

        <a href="#">AcessWealth</a>
        <p>Lorem ipsum dolor sit amet,</p>
        <!--add line here  -->
        <a href="#">WealthGuard</a>
        <p>Lorem ipsum dolor sit amet,</p>
         <!-- add line here -->
         <a href="#">WealthPoints</a>
         <p>Lorem ipsum dolor sit amet,</p>
           <!-- add line here -->

    </div>
    <div class="col-md-6">
        <span>01</span>
        <h2>AcessWealth</h2>
        <h3>"Reward yourself first"</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>
        <form>
          <div class="row">
            <div class="col">
              <input type="text" class="form-control" placeholder="Full name">
            </div>
            <div class="col">
              <input type="text" class="form-control" placeholder="E-mail address">
            </div>
          </div>
          <div class="row">
            <div class="col">
              <input type="text" class="form-control" placeholder="Mobile number">
            </div>
            <div class="col">
              <select  class="form-control">
            <option selected>Enquiry Topic</option>
            <option>Complaints</option>
            <option>Compliments</option>
          </select>
            </div>
          </div>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">Message</textarea>
            <button type="submit" class="btn">SUBMIT MESSAGE</button>
        </form>
    </div>
  </div>
</section>

  </main>


    <!-- Include: Main Footer -->
    <?php include '../inc/main-footer.php'; ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

    <!-- Custom Interface JS -->
    <script src="../js/interface.js?v=2.0"></script>

    <!-- Placeholder script - remove prior to going live -->
    <!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

  </body>
  </html>
