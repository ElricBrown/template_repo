<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../2.0/assets/img/favicon.png">

  <title>Constantia | Insurance made personal</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

  <!-- Base styles for this template -->
  <link href="../css/interface.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css?v=2.0" rel="stylesheet">

  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
</head>
<body>
  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../2.0/inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../2.0/inc/main-navigation.php'; ?>

  <!-- background-image to be added to the main -->
  <main>

<header>
  <div class="row">
<div class="col-md-6">
  <img src="http://www.ambledown.co.za/home_0_logo.gif" alt="">
  <h1>Health and Accident Underwriting Agency<span>.</span></h1>
  <p>Ambledown Financial Services is an Authorised Financial Services Company (FSP No. 10287), providing services of an Underwriting Management Agency. Ambledown is one of South Africa’s largest Health Underwriting Management Agency.Ambledown’s business model is built on a philosophy of innovation and focus on social macro-economic issues, it includes robust and efficient information technology platforms and a high service culture driven by great people.Ambledown provides underwriting management services and administration services to various insurers, including but not limited to Constantia Insurance Company Limited and Constantia Life and Health Assurance Company Limited.
</p>
  <a href="#">VISIT AMBLEDOWN WEBSITE</a>
</div>
<div class="col-md-6">
</div>
</div>
</header>

<section class="row">
  <div class="col">
    <span>01</span>
    <h2>Ambledown’s business model is built on a philosophy of innovation and focus on social macro-economic issues, it includes robust and efficient information technology platforms and a high service culture driven by great people.</h2>
    <div class="img-overlay-01">

    </div>
    <div class="img-overlay-02">

    </div>
  </div>

  <div class="text col">
    <h2>Ambledown is well recognised in South Africa as a leader in health insurance. This is exactly how and why: </h2>
    <p>We have established the industry that allows healthcare consultants to provide the additional essential cover for medical scheme members – Gap cover. This gained us a 60% market share. We created a range of products carefully balancing the benefits to ensure no harm is done to the general health risk pool (medical schemes) whilst ensuring that the cover provides a certainty of no financial disaster following a medical crisis.For the primary care products aimed at the less fortunate of our society, we entered into agreements with over 2,700 GP’s and Dentists with a discounted tariff, allowing us to provide an affordable alternative to treatment in a public facility.We are committed to a sustainable relationships with all our business partners, be it Insurers, brokers, administrators and healthcare advisors.Since its launch in 2004, Ambledown has grown consistently and considerably through innovation and client service.
</p>
    <a href="#">Visit the Ambledown Site</a>
  </div>
</section>
<!--
<section>
  <div class="row">
    <span>02</span>
    <h2>The DuePoint Team</h2>
  </div>
  <div class="row">
    <a href="#">VIEW MORE</a>
    <div class="director-img-01 col-md-3">
      <img src="" alt="">
      <h4>Brendan Benfield</h4>
      <p>Brendan is a registered Chartered Accountant and visiting fellow of Massachusetts...
      </p>
      <a href="#">LEARN MORE</a>

    </div>
    <div class="director-img-02 col-md-3">
      <img src="" alt="">
      <h4>Timothy Reynold</h4>
      <p>Timothy is a registered Chartered Accountant with over a decade of experience in the...</p>
      <a href="#">LEARN MORE</a>

    </div>
    <div class="director-img-03 col-md-3">
      <img src="" alt="">
      <h4>Rob van der Bijl</h4>
      <p>Rob was a co-founder of Wilred and has two decades of experinces in the digital marketing...</p>
      <a href="#">LEARN MORE</a>

    </div>
    <div class="director-img-04 col-md-3">
      <img src="" alt="">
      <h4>Stacey Paul</h4>
      <p>Stacey is a registered Chartered Accountant with both local and international experience across...</p>
      <a href="#">LEARN MORE</a>

    </div>
  </div>
</section>

<section>
  <div class="row">
    <div class="col-md-4">
    <span>03</span>
    <h2>DuePoint Products</h2> -->

        <!-- <a href="#">AcessWealth</a> -->
        <!-- <p>Lorem ipsum dolor sit amet,</p> -->
        <!--add line here  -->
        <!-- <a href="#">WealthGuard</a> -->
        <!-- <p>Lorem ipsum dolor sit amet,</p> -->
         <!-- add line here -->
         <!-- <a href="#">WealthPoints</a> -->
         <!-- <p>Lorem ipsum dolor sit amet,</p> -->
           <!-- add line here -->
<!--
    </div>
    <div class="col-md-6">
        <span>01</span>
        <h2>AcessWealth</h2>
        <h3>"Reward yourself first"</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>
        <form>
          <div class="row">
            <div class="col">
              <input type="text" class="form-control" placeholder="Full name">
            </div>
            <div class="col">
              <input type="text" class="form-control" placeholder="E-mail address">
            </div>
          </div>
          <div class="row">
            <div class="col">
              <input type="text" class="form-control" placeholder="Mobile number">
            </div>
            <div class="col">
              <select  class="form-control">
            <option selected>Enquiry Topic</option>
            <option>Complaints</option>
            <option>Compliments</option>
          </select>
            </div>
          </div>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">Message</textarea>
            <button type="submit" class="btn">SUBMIT MESSAGE</button>
        </form>
    </div>
  </div>
</section> -->

  </main>


    <!-- Include: Main Footer -->
    <?php include '../2.0/inc/main-footer.php'; ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

    <!-- Custom Interface JS -->
    <script src="../js/interface.js?v=2.0"></script>

    <!-- Placeholder script - remove prior to going live -->
    <!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

  </body>
  </html>
