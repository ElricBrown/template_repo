<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="assets/img/favicon.png">

  <title>Constantia | Insurance made personal</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

  <!-- Base styles for this template -->
  <link href="../css/interface.css" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css" rel="stylesheet">
  <!-- Custom media queries for this website -->
  <link href="../css/responsive.css" rel="stylesheet">
  <!-- Custom style for this website -->
  <link href="../css/bootstrap-grid.css" rel="stylesheet">

  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
</head>
<body>

  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../inc/main-navigation.php'; ?>

</div>

  <!-- background-image to be added to the main -->
  <main>
    <div class="row">

      <div class="col-md-6 bg-cover" style="background-image:url('../assets/img/contact/layer-21.png')">
        <h2>Contact Us</h2>
        <div class="row">
          <div class="col-md-6">
            <h3>Head Office</h3>
            <p>Tulbagh Office park</br>
              360 Oak Avenue</br>
              Randburg</br>
              2194</br>
              Johannesburg</br>
              South Africa
            </p>
            <p>
              <b>Telephone:</b>011 686 4200</br>
              <b>Facsimile:</b>011 789 8828</br>
              <b>E-mail:</b>info@constantiagroup.co.za
            </p>

            <!--add yellow line here  -->

            <h3>Postal Address</h3>
            <p>PO Box 3518</br>
              Cramerview</br>
              2060</br>
              Johannesburg</br>
              South Africa
            </p>
          </div>
          <div class="col-md-6">
            <h3>Cape Town Office</h3>
            <p>10 Dorp Street</br>
              Cape Town</br>
              8001</br>
              South Africa
            </p>
            <p>
              <b>Telephone:</b>021 424 8040</br>
              <b>Facsimile:</b>021 423 7995</br>
              <b>Toll Free Telephone:</b>0800 778 779
            </p>
            <!-- add yellow line here -->
            <h3>Office Hours</h3>
            <p>Monday - Friday: 08:00 - 17:00</p>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <h2>Send us a message</h2>
        <h3>Need more information? Get in touch with us...</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p>
        <form>
          <div class="row">
            <div class="col">
              <input type="text" class="form-control" placeholder="Full name">
            </div>
            <div class="col">
              <input type="text" class="form-control" placeholder="E-mail address">
            </div>
          </div>
          <div class="row">
            <div class="col">
              <input type="text" class="form-control" placeholder="Mobile number">
            </div>
            <div class="col">
              <select  class="form-control">
            <option selected>Enquiry Topic</option>
            <option>Complaints</option>
            <option>Compliments</option>
          </select>
            </div>
          </div>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">Message</textarea>
            <button type="submit" class="btn">Submit</button>
        </form>
      </div>

    </div>


  </main>


    <!-- Include: Main Footer -->
    <?php include '../inc/main-footer.php'; ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

    <!-- Custom Interface JS -->
    <script src="js/interface.js?v=2.0"></script>

    <!-- Placeholder script - remove prior to going live -->
    <!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

  </body>
  </html>
