<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="We acknowledge that people are our most valuable asset and that Constantia Group’s prosperity is directly influenced by the well-being of our employees. We believe that greatness is glimpsed by those who know where to look; we create opportunities that are real.">

  <meta name="title" content="Careers - Company Values, Growth and Development and Opportunities | Constantia Insurance Company">

  <link rel="icon" href="../assets/img/logos/favicon.png">

  <title>Careers - Company Values, Growth and Development and Opportunities | Constantia Insurance Company</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

  <!-- Base styles for this template -->
  <link href="../css/interface.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/bootstrap-grid.css" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/responsive.css" rel="stylesheet">


  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
</head>
<body>
  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../inc/main-navigation.php'; ?>

  <!-- background-image to be added to the main -->
  <main>
    <header>
      <h6>CAREERS</h6>
      <h1>CONSTANTIA COMPANY VALUES<span>.</span></h1>
      <p>Constantia Company is based on these core values</p>
      <ul>
        <li>Act with respect</li>
        <li>Excellence</li>
        <li>Integrity</li>
        <li>Adding value</li>
        <li>Accountabilty</li>
        <li>Teamwork</li>
      </ul>
    </header>

    <section class="constantia-story container-fluid text-center">
      <h2><span>01</span> Employee Wellness</h2>
      <p>We acknowledge that people are our most valuable asset and that our organisation’s prosperity is directly influenced by the well-being of our employees.  We will continue looking at new innovative ways to ensure the wellbeing of each and every individual working within our group.</p>
      <a href="#">Employee Wellness Magazine</a>
    </section>

    <section>
      <div class="">
        <h2>Growth and Development</h2>
        <h3>Celebrating 60 years of success</h3>
        <p><b>1952</b> - Constantia Insurance Company Limited was initially founded. Then, it was known as The Marine and Trade Insurance Company Ltd. Since then the company profile has changed from that of a "traditional" insurer, that is, an insurance company that deals directly with customers or directly with brokers, to a company that deals mainly with Underwriting Managers and Administrators.</p>
        <p><b>2003</b> - Conduit Risk and Insurance Holdings (Proprietary) Limited ("CRIH"), a wholly owned subsidiary of Conduit Capital Limited ("Conduit"), aquired Constantia Insurance Company Limited in 2003.</p>
        <p><b>2006</b> - Constantia Insurance Limited Company, together with Constantia Life Limited (previously Goodall & Bourne) and Constantia Life & Health Limited, formed the main business of Constantia Risk Insurance Holdings.</p>
        <p><b>2012</b> - Although a lot has changed since 1952, our core values have remained the same. Our philosophy of partnership, support and personalised service, has made us an insurer who lives by personal service.</p>
      </div>
    </section>


    <section>
      <h2>Opportunities</h2>
      <p>We now know that success goes beyond the old school tie, beyond the MBA, beyond being in-the-know and even beyond the bottom line.  We believe that greatness is glimpsed by those who know where to look; brushed by those who reach out; but only felt by those who manage to combine instinct with knowledge. To create opportunities that are real. And making us all feel as if we truly belong.</p>
      <h3>Software Programme Developer</h3>
      <ul>
        <li>Matric with a minimum of 3 year's solid work related development experience  in the workplace, with insurance product building and support experience (tertiary exposure excluded). Remuneration package will be tailored according to the relevant technical experience.</li>
        <li>Programming certification required.</li>
        <li>Must have database experience.</li>
        <li>Microsoft Windows Desktop experience required.</li>
        <li>Ideally an IT Diploma or related Degree will be an advantage but is not essential.</li>
        <li>Good communicator and ability to support user issues.</li>
        <li>Experience working in an Agile development environment preferred but not essential.</li>
      </ul>
      <h4>Qualities</h4>
      <ul>
        <li>Good command of the English Language.</li>
        <li>Perseverance and determination.</li>
        <li>Integrity, honesty.</li>
        <li>Assertive.</li>
        <li>Resilience.</li>
        <li>Team player.</li>
        <li>Good communication, good listener.</li>
        <li>Time management.</li>
      </ul>

      <h3>SQL Database Administrator</h3>
      <h5>JOB PURPOSE</h5>
      <p>To achieve the Company’s vision of “Insurance made Personal”. </p>
      <p>To assist with data extraction, managing the company data, working with the developers to create strong robust database designs. Integrating data from various sources UMA and divisions and develop scripts for reporting and business intelligence. In addition the incumbent will also assist with software development, data mining and data integration to multiple systems. </p>
      <h4>PRINCIPAL RESPONSIBILITIES</h4>
      <ul>
        <li>Object Orientated Languages required.</li>
        <li>Good understanding of databases and data integration.</li>
        <li>Must be able to write complex data scripts and stored procedures.</li>
        <li>Must be Focused and pay attention to detail.</li>
        <li>MS_SQL experience essential.</li>
        <li>Must have insurance product support and build experience.</li>
        <li>Exposure to programming and scripting.</li>
        <li>Knowledge of Debugging software.</li>
        <li>Able to prepare training material and offer specialized software training.</li>
        <li>Able to write and document systems.</li>
        <li>Visual Studio experience</li>
      </ul>
      <h4>KEY PERFORMANCE AREAS</h4>
      <ul>
        <li>Provide external and internal support on all software application with primary focus on Cardinal Insurance system.</li>
        <li>Able to write complex database queries.</li>
        <li>Ability to write programs to import data and transactions in the applications.</li>
        <li>Capable to writing Stored procedures.</li>
        <li>Ability to manage large data.</li>
        <li>Good report writing skills.</li>
        <li>Ability to write API interfaces to act with other third-party databases.</li>
        <li>Maintain software components and ensure reliable deployment of new features.</li>
        <li>Provide ongoing user support and training.</li>
        <li>Set-up and deploy database releases to clients' QA, UAT and live environments.</li>
        <li>Documenting, troubleshooting and problem resolution steps independently.</li>
        <li>Responsible for collaborating with a variety of individuals and teams at all levels within the organization.</li>
        <li>Ability to understand MS _SQL databases, write queries extract data.</li>
        <li>Perform advanced root cause analysis on bugs and databases.</li>
        <li>Configure and maintain in house scripting.</li>
        <li>Automate and implement processes.</li>
      </ul>
      <h4>EXPERIENCE, KNOWLEDGE AND SKILLS</h4>
      <ul>
        <li>Matric with a minimum of 3 year’s solid work related development experience in the workplace, with insurance product building and support experience (tertiary  exposure excluded). Remuneration package will be tailored according to the relevant technical experience.</li>
        <li>Programming certification required.</li>
        <li>Must have database experience.</li>
        <li>Microsoft Windows Desktop experience required.</li>
        <li>Ideally an IT Diploma or related Degree will be an advantage but is not essential.</li>
        <li>Good communicator and ability to support user issues.</li>
        <li>Experience working in an Agile development environment preferred but not essential.</li>
      </ul>
      <h4>Qualities</h4>
      <ul>
        <li>Good command of the English Language.</li>
        <li>Perseverance and determination.</li>
        <li>Integrity, honesty.</li>
        <li>Assertive.</li>
        <li>Resilience.</li>
        <li>Team player.</li>
        <li>Good communication, good listener.</li>
        <li>Time management.</li>
      </ul>
      <h3>Keep in touch with the latest available positions at Constantia on:</h3>
      <a href="https://www.facebook.com/insmadepersonal/" target="_blank" class="orange p-2"><i class="fab fa-facebook-square"></i></a>
      <a href="https://twitter.com/@InsMadePersonal" target="_blank" class="orange p-2"><i class="fab fa-twitter"></i></a>
      <ul>
        <li><a href="">Job Opportunity - Internship Programme (Conduit Capital Limited)</a></li>
        <li><a href="">Job Opportunity - Senior Manager: Product and Portfolio Development</a></li>
        <li><a href="">Job Opportunity - Corporate Governance Officer</a></li>
        <li><a href="">Job Opportunity - Risk Manager</a></li>
      </ul>
    </section>

    <section>
      <div class="row">
        <div class="col-md-6">
          <h2>Why Work at Constantia</h2>
          <h4><i><b>While we know that wisdom comes with six decades of carefully studied experience, it means nothing without a mind hungry for new knowledge. We will remain a company of people who seek out new ways of doing things while not losing sight of the old.</b></i></h4>
          <p>We are obsessed with detail. It’s because we believe that taking care of detail so often makes all the difference. It’s a properly knotted tie. It’s a perfectly prepared cup of tea. It’s in a warmly presented handshake and remembering someone’s name – whether you saw them yesterday or a decade ago. It’s in knowing your next appointment will be expecting a thorough meeting, but would appreciate their favourite newspaper left for them in the reception area beforehand. And it’s in preparing a contract on time, but knowing what the right hour is to present it.</p>
          <p>To our mind, there are three things that set us apart from our competitors and continues to drive our business forward. First and foremost is our people. As a service business, we realised that it is the commitment, skills and vision of our people that sets us apart. Our people are highly skilled at what they do and always have one eye on the horizon. Not everyone in the organisation can have the most prestigious job, but everyone can take their role seriously and do it well.</p>
          <p>We spend a considerable amount of effort ensuring our continued agility. Because of the pace at which business moves, only the most nimble companies stay ahead of the pack. We will keep succeeding because we are able to swiftly develop unique services and tailor-made innovation.</p>
          <p>Yet you can’t be nimble without strongly believing in simplicity. Our systems have been created with efficiency in mind. That means all our systems actually work, work fast and work well. This has been particularly important in the context of the insurance industry’s increasingly tight regulatory framework.</p>
          <p>What is the real point of having these highly skilled people and agile way of doing business and simple systems to ensure our business gets done well? It means all of us accept a responsibility to look further. At Constantia we never lose sight of the larger vision. We never stop looking for new opportunities. And that is what we mean by “insurance made personal”.</p>
        </div>
        <div class="col-md-6">
          <img src="http://via.placeholder.com/300" alt="">
          <img src="http://via.placeholder.com/300" alt="">
          <img src="http://via.placeholder.com/300" alt="">
          <img src="http://via.placeholder.com/300" alt="">


        </div>
      </div>
    </section>

  </main>


  <!-- Include: Main Footer -->
  <?php include '../inc/main-footer.php'; ?>


  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
  <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
  <script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

  <!-- Custom Interface JS -->
  <script src="../js/interface.js?v=2.0"></script>

  <!-- Placeholder script - remove prior to going live -->
  <!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

</body>
</html>
