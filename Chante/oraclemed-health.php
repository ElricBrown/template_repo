<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../2.0/assets/img/favicon.png">

  <title>Constantia | Insurance made personal</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

  <!-- Base styles for this template -->
  <link href="../css/interface.css?v=2.0" rel="stylesheet">
  <!-- Custom styles for this website -->
  <link href="../css/theme.css?v=2.0" rel="stylesheet">

  <!-- DEBUG MODE -->
  <?php if (isset($_GET['debug'])) { echo '<link href="assets/css/debug.css" rel="stylesheet">'; } ?>

  <!-- HOTJAR http://makebetter.co.za/beta/constantia/ -->
  <script>
  (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:845543,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
</head>
<body>
  <!-- Include: Top Logo/Menu Bar -->
  <?php include '../2.0/inc/main-topbar.php'; ?>
  <!-- Include: Main Overlay Menu -->
  <?php include '../2.0/inc/main-navigation.php'; ?>

  <!-- background-image to be added to the main -->
  <main>

<header>
  <div class="row">
<div class="col-md-6">
  <img src="http://www.oraclemed.com/templates/home/images/header.jpg" alt="">
  <h1>Oraclemed Health (Pty) Ltd<span>.</span></h1>
  <p>Our mission is to create valuable and enduring relationships based on the highest levels of integrity and good faith.ia Insurance Company Limited and Constantia Life and Health Assurance Company Limited.
</p>
  <a href="#">VISIT ORACLEMED WEBSITE</a>
</div>
<div class="col-md-6">
</div>
</div>
</header>

<section class="row">
  <div class="col">
    <span>01</span>
    <h2>Our mission is to create valuable and enduring relationships based on the highest levels of integrity and good faith.</h2>

  </div>

  <div class="text col">
    <h2>Our growth has been built on our genuine commitment to provide our clients with professional expertise and healthcare solutions to meet the challenges and complex needs of our international client base.</h2>
    <p>Oraclemed Health was established in 2001 and provides flexible international Health Insurance Products to ex-patriots and third party nationals resident permanently or temporality in African countries other than the Republic of South Africa. They provide members with access to a worldwide network of hospitals/medical providers, which will allow for direct settlement of claims. Should treatment be required, their Call Centre will ensure that you are directed to the appropriate hospital. You will have access to 24 hour emergency assistance and a medically staffed international helpline. All medical events are individually case-managed.
</p>
    <a href="#">Visit the Ambledown Site</a>
  </div>
</section>
  </main>


    <!-- Include: Main Footer -->
    <?php include '../2.0/inc/main-footer.php'; ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

    <!-- Custom Interface JS -->
    <script src="../js/interface.js?v=2.0"></script>

    <!-- Placeholder script - remove prior to going live -->
    <!-- <script src="https://getbootstrap.com/assets/js/vendor/holder.min.js"></script> -->

  </body>
  </html>
