<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>


    <h2>Page List</h2>
<!--Home-->
    <ul>
      <li><a href="index.php" target="_blank">Home</a></li>
      <!--Insurances-->
      <li>Insurances
        <ul>
          <!--Page for all insurances//temp-->
          <li><a href="types-of-insurances/insurances.php" target="_blank">Insurances</a></li>
      </ul>
<!--brands-->
      <li>Brands
        <ul>
          <li><a href="brands/admin-plus.php" target="_blank">Admin Plus</a></li>
          <li><a href="brands/ambledown.php" target="_blank">Ambledown</a></li>
          <li><a href="brands/duepoint.php" target="_blank">Duepoint</a></li>
          <li><a href="brands/ethiQal.php" target="_blank">EthiQal</a></li>
          <li><a href="brands/fosho.php" target="_blank">Fosho</a></li>
          <li><a href="brands/insure-africa.php" target="_blank">Insure Africa</a></li>
          <li><a href="brands/jasure.php" target="_blank">Jasure</a></li>
          <li><a href="brands/legal-defender.php" target="_blank">Legal Defender</a></li>
          <li><a href="brands/metology.php" target="_blank">Metology</a></li>
          <li><a href="brands/mobility.php" target="_blank">Mobility</a></li>
          <li><a href="brands/oraclemed-health.php" target="_blank">Oraclemed Health</a></li>
          <li><a href="brands/pmd.php" target="_blank">PMD</a></li>
          <li><a href="brands/sgi.php" target="_blank">SGI</a></li>
          <li><a href="brands/tui.php" target="_blank">TUI</a></li>
        </ul>
      </li>

<!--about-->
      <li>About
        <ul>
          <li><a href="about/who-we-are.php" target="_blank">Who we are</a></li>
          <li><a href="about/our-directors.php" target="_blank">Our Directors</a></li>
        </ul>
      </li>

<!--news-->
    <li>Latest News
      <ul>
        <li><a href="latest-news/policies.php" target="_blank">Policies</a></li>
        <li><a href="latest-news/latest-news.php" target="_blank">Latest News</a></li>
        <li><a href="latest-news/updates.php" target="_blank">Updates</a></li>
    </ul>
  </li>
<!--careers-->
    <li>Careers
      <ul>
        <li><a href="careers/careers.php" target="_blank">Careers</a></li>
      </ul>
    </li>

    <!--contact-->
    <li>Contact
      <ul>
          <li><a href="contact/contact-us.php" target="_blank">Contact Us</a></li>
        </ul>
      </li>

<!--terms and conditons-->
      <li><a href="terms-and-conditions.php" target="_blank">Terms and Conditions</a></li>
</ul>

  </body>
</html>
